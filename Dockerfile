FROM gitlab-registry.cern.ch/linuxsupport/c8-base AS Build

RUN dnf install -y sudo git make python3 'dnf-command(config-manager)' 'dnf-command(builddep)' && dnf clean all
RUN dnf config-manager --enable PowerTools
RUN dnf builddep -y rpm dnf && dnf clean all

COPY build.sh /work/build.sh
WORKDIR work
RUN  /work/build.sh

# Final image

FROM gitlab-registry.cern.ch/linuxsupport/c8-base
RUN  dnf install -y python3 && dnf clean all
COPY --from=Build /usr/local/ /usr/local
COPY  adnf /usr/local/bin
COPY  dnf.conf /usr/local/etc/dnf/dnf.conf
COPY  yum.repos.d /usr/local/etc/yum.repos.d/
