#!/bin/bash

# Prerequisites
#
# sudo dnf config-manager --enable PowerTools
# sudo dnf builddep -y rpm dnf


# You can export this variable beforehand
# e.g. to $HOME/local

# DESTDIR=${DESTDIR:=/usr/local}

# Get ATLAS specific versions
git clone --depth 1 -b atlas https://gitlab.cern.ch/rhauser/rpm.git
(cd /usr/local; git clone --depth 1 -b atlas https://gitlab.cern.ch/rhauser/dnf.git)

## Build RPM

pushd rpm

export PYTHON=python3

./autogen.sh \
   --enable-python \
   --with-vendor=ATLAS \
   --with-crypto=openssl \
   --without-lua

make
pushd python
$PYTHON setup.py build
popd
make install
pushd python
$PYTHON setup.py install --skip-build 
popd

popd

## Build DNF

pushd /usr/local/dnf
mkdir build
pushd build
DESTDIR=/usr/local
cmake -DPYTHON_DESIRED="3" -D WITH_MAN=0 ..
make
popd

popd
