# atlas-dnf

Build a patched version of RPM and DNF that allows to install RPMs without root access and arbitrary relocations (if the RPMs are prepared for it).